!

!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname ESW2
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
memory-size iomem 5
ip cef
!
!
!
!
ipv6 unicast-routing
ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
archive
 log config
  hidekeys
! 
!
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface Serial0/0
 no ip address
 shutdown
 no fair-queue
 clock rate 2000000
!
interface FastEthernet0/1
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface Serial0/1
 no ip address
 shutdown
 clock rate 2000000
!
interface FastEthernet1/0
!
interface FastEthernet1/1
!
interface FastEthernet1/2
!
interface FastEthernet1/3
!
interface FastEthernet1/4
!
interface FastEthernet1/5
!
interface FastEthernet1/6
!
interface FastEthernet1/7
!
interface FastEthernet1/8
!
interface FastEthernet1/9
!
interface FastEthernet1/10
!
interface FastEthernet1/11
!
interface FastEthernet1/12
!
interface FastEthernet1/13
!
interface FastEthernet1/14
!
interface FastEthernet1/15
!
interface Vlan1
 ip address 192.0.2.1 255.255.255.0
 ipv6 address 2001:DB8:F::F/64
!
router bgp 65500
 bgp router-id 22.22.22.22
 bgp log-neighbor-changes
 neighbor 2001:DB8:F::2 remote-as 65500
 neighbor 192.0.2.2 remote-as 65500
 !
 address-family ipv4
  no neighbor 2001:DB8:F::2 activate
  neighbor 192.0.2.2 activate
  no auto-summary
  no synchronization
 exit-address-family
 !
 address-family ipv6
  neighbor 2001:DB8:F::2 activate
 exit-address-family
!
ip forward-protocol nd
ip route 10.0.0.0 255.0.0.0 192.0.2.2
!
!
ip http server
no ip http secure-server
!
ipv6 route 2001:DB8::/32 2001:DB8:F::2
!
!
!
!
!
!
control-plane
!
!
!
!
!
!
!
!
!
!
line con 0
line aux 0
line vty 0 4
 login
!
!
end
