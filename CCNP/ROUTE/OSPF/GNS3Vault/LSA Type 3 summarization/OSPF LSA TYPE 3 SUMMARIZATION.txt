OSPF LSA TYPE 3 SUMMARIZATION
WRITTEN BY RENE MOLENAAR ON 30 AUGUST 2011. POSTED IN OSPF
SCENARIO:
The mineral express is specialized in trading expensive minerals. You are their senior network engineer and responsible for the OSPF network that they have running. One of your colleagues is complaining that the routing table is becoming too large. It's up to you to configure some summarization!
GOAL:
* All IP addresses have been preconfigured for you.
* Configure OSPF on all routers. Achieve full connectivity.
* Router Talc and Fluorite have multiple loopback interfaces. Configure the network so Area 0 only sees a summary of these networks. Use the most optimal summary.
IOS:
c3640-jk9s-mz.124-16.bin

