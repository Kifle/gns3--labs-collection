hostname Rack1SW2
!
enable password cisco
!
ip subnet-zero
no ip domain-lookup
!
interface FastEthernet1/0
 description To BB2
!
interface FastEthernet1/2
 description To R2 F0/0 
!
interface FastEthernet1/4
 description To R4 E0/0
!
interface FastEthernet1/6
 description To R6 E0/0
!
interface FastEthernet1/7
 description To SW1 F1/7
!
interface FastEthernet1/8
 description To SW1 F1/8
!
interface FastEthernet1/9
 description To SW1 F1/9
!
interface FastEthernet1/10
 description To SW3 F1/10
!
interface FastEthernet1/11
 description To SW3 F1/11
!
interface FastEthernet1/12
 description To SW3 F1/12
!
interface FastEthernet1/13
 description To SW4 F1/10
!
interface FastEthernet1/14
 description To SW4 F1/11
!
interface FastEthernet1/15
 description To SW4 F1/12
!
ip classless
!
line con 0
 exec-timeout 0 0
 logging synchronous
 privilege level 15
line aux 0
 exec-timeout 0 0
 privilege level 15
line vty 0 4
 login
 password cisco
